#! /usr/bin/env bash
#
# USAGE
# ./colorSepatation.sh input.pdf
#
# OUTPUT
# a directory called `tiffsep` where you have a jpg image per color channel per page

rm -r pages 2> /dev/null 

PROJECTDIR=`pwd`
bn=$(basename ${1} .pdf)
PLATESDIR=${PROJECTDIR}/build/pdf/cmyk/${bn}_plates

# Change resolution here, -r150x150 = 150dpi
gs -dSimulateOverprint=true -sDEVICE=tiffsep -dNOPAUSE -dBATCH -r150x150 -sOutputFile=${bn}-page%03d.tif ${1}

echo "MOVE TIF FILES INTO '${PLATESDIR}' FOLDER"
mkdir -p ${PLATESDIR}
mv ${bn}*.tif ${PLATESDIR}
cd ${PLATESDIR}

echo "CONVERT TIF TO JPG"
for TIF in *.tif; do convert $TIF $(basename $TIF .tif).jpg; done

echo "REMOVE TIF FILES"
rm *tif

echo "MAKE COLOURED PREVIEW"
for Cyan in *Cyan*; do convert $Cyan -colorspace Gray +level-colors cyan, $Cyan; done
for Magenta in *Magenta*; do convert $Magenta -colorspace Gray +level-colors magenta, $Magenta; done
for Yellow in *Yellow*; do convert $Yellow -colorspace Gray +level-colors yellow, $Yellow; done



echo "GENERATE HTML PREVIEW PAGE"


cat ${PROJECTDIR}/bin/utils/colorSeparation_header.html > ${PLATESDIR}/00-${bn}-plates.html

start=1
end=$(( $(ls | wc -l) /5))

size=$(identify ${bn}-page001.jpg |  cut  -d " " -f 3)
width=$(echo ${size} | cut -d "x" -f 1)
height=$(echo ${size} | cut -d "x" -f 2)

for i in $(eval echo "{$start..$end}")
do
    page=$(printf "%03u" ${i})
    echo "<div id='page${page}' class='page' style='height: ${height}px; width: ${width}px;'>" >> ${PLATESDIR}/00-${bn}-plates.html
    echo "    <img class='all' style='height: ${height}px; width: ${width}px;' src='${bn}-page${page}.jpg' />" >> ${PLATESDIR}/00-${bn}-plates.html
    echo "    <img class='cyan' style='height: ${height}px; width: ${width}px;' src='${bn}-page${page}(Cyan).jpg' />" >> ${PLATESDIR}/00-${bn}-plates.html
    echo "    <img class='magenta' style='height: ${height}px; width: ${width}px;' src='${bn}-page${page}(Magenta).jpg' />" >> ${PLATESDIR}/00-${bn}-plates.html
    echo "    <img class='yellow' style='height: ${height}px; width: ${width}px;' src='${bn}-page${page}(Yellow).jpg' />" >> ${PLATESDIR}/00-${bn}-plates.html
    echo "    <img class='black' style='height: ${height}px; width: ${width}px;' src='${bn}-page${page}(Black).jpg' />" >> ${PLATESDIR}/00-${bn}-plates.html
    echo "</div>" >> ${PLATESDIR}/00-${bn}-plates.html
done

cat ${PROJECTDIR}/bin/utils/colorSeparation_footer.html  >> ${PLATESDIR}/00-${bn}-plates.html
